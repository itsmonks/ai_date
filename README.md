# Simple code to help you get a date
1. Install the requirements
2. Add your images to the images folder for best image, start them with `me_`
3. Run `python test_helper.py`

This is a pretty simple fork of:
https://github.com/gustavz/AttractiveNet

Shout out to them for their great work!