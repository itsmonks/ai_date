import numpy as np
import cv2
from tensorflow.keras.models import load_model
from os import system, listdir
from os.path import isfile, join

import helper

def best_image(model):
    images = [f for f in listdir('images/') if isfile(join('images/', f))]
    me = [img for img in images if 'me' in img]
    best_pic = me[0]
    highest_score = 0
    for pic in me:
        img = cv2.imread('images/' + pic)
        score = model.predict(np.expand_dims(helper.preprocess_image(img,(350,350)), axis=0))
        if score > highest_score:
            highest_score = score
            best_pic = pic
    final_result = cv2.imread('images/' + best_pic)
    score_text = f'Score: {str(round(highest_score[0][0],1))}'
    cv2.putText(
        final_result, 
        score_text, 
        (10,50), 
        cv2.FONT_HERSHEY_SIMPLEX, 
        1, 
        (255,0,0), 
        2, 
        cv2.LINE_AA
    )
    cv2.imshow(best_pic, final_result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def individual_image(model):
    while(True):
        img_name = input("What image would you like to use? ")
        img = cv2.imread('images/' + img_name + '.jpg')
        cv2.imshow(img_name, img)
        score = model.predict(np.expand_dims(helper.preprocess_image(img,(350,350)), axis=0))
        score_text = f'Score: {str(round(score[0][0],1))}'
        cv2.putText(
            img, 
            score_text, 
            (10,50), 
            cv2.FONT_HERSHEY_SIMPLEX, 
            1, 
            (255,0,0), 
            2, 
            cv2.LINE_AA
        )
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    model_name = 'attractiveNet_mnv2'
    model_path = 'models/' + model_name + '.h5'

    model = load_model(model_path)

    system('cls') # Clear screen before running for better output
    best_image(model)
    # individual_image(model)
    # cap = cv2.VideoCapture(0)

    # while(True):
    #     ret, frame = cap.read()
    #     score = model.predict(np.expand_dims(helper.preprocess_image(frame,(350,350)), axis=0))
    #     text1 = f'AttractiveNet Score: {str(round(score[0][0],1))}'
    #     text2 = "press 'Q' to exit"
    #     cv2.putText(frame,text1, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,0), 2, cv2.LINE_AA)
    #     cv2.putText(frame,text2, (10,100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,0), 2, cv2.LINE_AA)
    #     cv2.imshow('AttractiveNet',frame)
    #     if cv2.waitKey(1) & 0xFF == ord('q'):
    #         break

    # cap.release()
    # cv2.destroyAllWindows()